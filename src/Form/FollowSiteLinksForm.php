<?php

namespace Drupal\follow\Form;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\State\StateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form handler for adding follow site links.
 */
class FollowSiteLinksForm extends FormBase {

  /**
   * Returns the config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $config;

  /**
   * Returns the state service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * Constructs a FollowSiteLinksForm form.
   *
   * @param \Drupal\Core\Config\ConfigFactory $config
   *   Defines the configuration object factory.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state service.
   */
  public function __construct(ConfigFactory $config, StateInterface $state) {
    $this->config = $config;
    $this->state = $state;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('state'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'follow_site_links_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $links = follow_get_follow_links();
    $data = (array) $this->state->get('follow_site_links');

    $form['links'] = [
      '#type' => 'html_tag',
      '#tag' => 'div',
      '#tree' => TRUE,
    ];

    foreach ($this->config->get('follow.settings')->get('links') as $link) {
      if (isset($links[$link])) {
        $form['links'][$link] = [
          '#type' => 'textfield',
          '#title' => $links[$link]['label'],
          '#default_value' => !empty($data[$link]) ? $data[$link] : '',
        ];
      }
    }

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->state->set('follow_site_links', $form_state->getValue('links'));
    Cache::invalidateTags(['follow_site_links']);

    $this->messenger()->addStatus($this->t('Follow site links have been saved.'));
  }

}
