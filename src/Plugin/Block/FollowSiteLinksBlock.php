<?php

namespace Drupal\follow\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\follow\FollowManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Block with the follow site links.
 *
 * @Block(
 *   id = "follow_site_links_block",
 *   admin_label = @Translation("Follow site links"),
 *   category = @Translation("System")
 * )
 */
class FollowSiteLinksBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Returns the follow manager service.
   *
   * @var \Drupal\follow\FollowManagerInterface
   */
  protected $followManager;

  /**
   * Constructs a FollowSiteLinks block.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\follow\FollowManagerInterface $follow_manager
   *   Defines the follow service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, FollowManagerInterface $follow_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->followManager = $follow_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('follow.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'size' => 'small',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form['size'] = [
      '#type' => 'radios',
      '#title' => $this->t('Icon size'),
      '#default_value' => $this->configuration['size'],
      '#options' => [
        'small' => $this->t('Small'),
        'large' => $this->t('Large'),
        'paulrobertlloyd32' => $this->t('Paul Robert Lloyd 32'),
      ],
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['size'] = $form_state->getValue('size');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];
    $build['follow_links'] = $this->followManager
      ->getSiteLinksIcons($this->configuration['size']);
    return $build;
  }

}
